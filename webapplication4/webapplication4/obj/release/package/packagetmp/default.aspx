﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>雷烨的冒险 2019</title>
<!--这是播放器的-->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css1/demo.css" />
<!--必要样式-->
<link rel="stylesheet" href="css1/audioplayer.css" />

</head>

<body>
<!--这是我的主样式-->
<script type="text/javascript" src="script/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="script/jquery-ui-1.10.2.custom.min.js"></script>
<script type="text/javascript" src="script/detect-browser-device.js"></script>
<link href="fancybox.css" rel="stylesheet" />
<script src="fancyjs1.js"></script>
    <script src="fancyjs2.js"></script>

<!--我也不知道是什么-->
<noscript>
   <div id="javascript-banner-container">
            <div id="javascript-banner">
                <div id="javascript-banner-left"></div>
                <div id="javascript-banner-middle">
                    <div id="javascript-banner-text-a">PLEASE TURN ON YOUR</div>
                    <div id="javascript-banner-text-b">JAVASCRIPT</div>
                </div>
                <div id="javascript-banner-right"></div>
            </div>
        </div> 
</noscript>


<div id="content" class="content-noscroll">


    <div id="preloader" class="displaynone">
    <script type="text/javascript" src="script/preloader-transparent-or-displaynone.js"></script>
    
    <!--Loading-->
        <div id="preloader-banner-container">
            <div id="preloader-banner">
                <div id="preloader-banner-left"></div>
                <div id="preloader-banner-middle">
                    <div id="preloader-banner-text-a">LOADING</div>
                    <div id="preloader-dots" class="preloader-dots-animation"></div>
                </div>
                <div id="preloader-banner-right"></div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="script/preloader.js"></script>


    <div id="page"></div>
    
    
    <div id="container" class="displaynone">
    <script type="text/javascript" src="script/container-transparent-or-displaynone.js"></script>
    
        <div id="layer-vertical-1" class="layer-vertical">
        	<div id="cloud-5" class="cloud"></div>
            <div id="cloud-6" class="cloud"></div>
            <div id="cloud-7" class="cloud"></div>
            <div id="cloud-8" class="cloud"></div>
            <div id="cloud-9" class="cloud"></div>
      	</div>
        
        <div id="layer-vertical-2" class="layer-vertical">
            <div id="ground-and-grass-container-2">
                <div class="ground"></div>
                <div class="grass"></div>
                <div id="waterfall-1" class="waterfall"></div>
                <div id="waterfall-2" class="waterfall"></div>
            </div>
        </div>
        
		<!--layer-vertical-3 is at the bottom. it will be in front of layer horizontal, so the button on the banner in layer vertical will not be block by layer horizontal-->
        
        <div id="layer-horizontal-1" class="layer-horizontal">
            <div id="cloud-1" class="cloud"></div>
            <div id="cloud-2" class="cloud"></div>
            <div id="cloud-3" class="cloud"></div>
            <div id="cloud-4" class="cloud"></div>
            
            <div id="sea-layer-horizontal-1" class="sea">
            	<div class="sea-seal"></div>
            </div>
            
            <div id="coral-big-1" class="coral-big"></div>
            <div id="coral-big-2" class="coral-big"></div>
        </div>
        
        <div id="layer-horizontal-2" class="layer-horizontal">
        	<div id="mountain-1" class="mountain"></div>
            <div id="mountain-2" class="mountain"></div>
            
            <div id="coral-1" class="coral-a"></div>
          	<div id="coral-2" class="coral-b"></div>
          	<div id="coral-3" class="coral-a"></div>
            
          	<div id="crane-1" class="crane"></div>
            <div id="hangar">
            	<div id="hangar-door"></div>
                <div id="hangar-window"></div>
                <div id="hangar-roof"></div>
            </div>
            <div id="crane-2" class="crane"></div>
        </div>
        
        <div id="layer-horizontal-3" class="layer-horizontal">
        
            <div id="plants-container">
                <div id="plant-text-1" class="plant-text">140个</div>
                <div id="plant-text-2" class="plant-text">110个</div>
                <div id="plant-text-3" class="plant-text">80个</div>
                <div id="plant-text-4" class="plant-text">60个</div>
                <div id="plant-text-5" class="plant-text">40个</div>
                
                <div id="plant-line-1" class="plant-line"></div>
                <div id="plant-line-2" class="plant-line"></div>
                <div id="plant-line-3" class="plant-line"></div>
                <div id="plant-line-4" class="plant-line"></div>
                <div id="plant-line-5" class="plant-line"></div>
                
                <div id="plant-1" class="plant">
                    <div class="plant-stalk"></div>
                    <div class="plant-head-leaves"></div>
                </div>
                
                <div id="plant-2" class="plant">
                    <div class="plant-stalk"></div>
                    <div class="plant-head-leaves"></div>
                </div>
                
                <div id="plant-3" class="plant">
                    <div class="plant-stalk"></div>
                    <div class="plant-head-leaves"></div>
                </div>
                
                <div id="plant-4" class="plant">
                    <div class="plant-stalk"></div>
                    <div class="plant-head-leaves"></div>
                </div>
                
                <%--<div id="plant-ribbon-container">
                    <div id="plant-ribbon-1" class="plant-ribbon">
                        <div class="plant-ribbon-left"></div>
                        <div class="plant-ribbon-middle">DESIGN</div>
                        <div class="plant-ribbon-right"></div>
                    </div>
                    
                    <div id="plant-ribbon-2" class="plant-ribbon">
                        <div class="plant-ribbon-left"></div>
                        <div class="plant-ribbon-middle">ILLUSTRATION</div>
                        <div class="plant-ribbon-right"></div>
                    </div>
                   
                    <div id="plant-ribbon-3" class="plant-ribbon">
                        <div class="plant-ribbon-left"></div>
                        <div class="plant-ribbon-middle">CODE</div>
                        <div class="plant-ribbon-right"></div>
                    </div>
                   
                    <div id="plant-ribbon-4" class="plant-ribbon">
                        <div class="plant-ribbon-left"></div>
                        <div class="plant-ribbon-middle">ANIMATION</div>
                        <div class="plant-ribbon-right"></div>
                    </div>
                </div>--%>
   
                
                <div class="ribbon-container">
                    <div class="ribbon-relative">
                        <div class="ribbon-left"></div>
                        <div class="ribbon-middle">我跳绳进步了100个</div>
                        <div class="ribbon-right"></div>
                    </div>
                </div>
            </div>
            
            
            <div id="elevation-1" class="elevation">
                <div class="ground"></div>
                <div class="grass"></div>
            </div>
            
            
            <div id="tree-9" class="tree-bright-a"></div>
            <div id="tree-10" class="tree-dark-c"></div>
            
            <div id="title-about"></div>
            
            <div id="tree-7" class="tree-dark-a"></div>
            <div id="tree-8" class="tree-bright-a"></div>
            
            <div id="gate-1" class="gate">
                <div id="gate-text-1" class="gate-text">运动</div>
            </div>
            
            <div id="gate-2" class="gate">
                <div id="gate-text-2" class="gate-text">才艺</div>
            </div>
            
            
            <div id="nba-container">
            	<div id="tree-20" class="tree-bright-b"></div>
                <div id="tree-21" class="tree-dark-d"></div>
                <div id="tree-22" class="tree-bright-b"></div>
            
            	<div id="tree-23" class="tree-dark-b"></div>
                <div id="tree-24" class="tree-bright-e"></div>
                <div id="tree-25" class="tree-dark-b"></div>
                
                <div id="nba-rim-container">
                    <div id="nba-rim"></div>
                </div>
                <div id="nba-board-1" class="nba-board-blue">
                    <div class="nba-text-container">
                        <div class="nba-text">N</div>
                    </div>
                </div>
                <div id="nba-board-2" class="nba-board-blue">
                    <div class="nba-text-container">
                        <div class="nba-text">B</div>
                    </div>
                </div>
                <div id="nba-board-3" class="nba-board-blue">
                    <div class="nba-text-container">
                        <div class="nba-text">A</div>
                    </div>
                </div>
                <div id="nba-board-4" class="nba-board-red">
                    <div class="nba-text-container">
                        <div class="nba-text">F</div>
                    </div>
                </div>
                <div id="nba-board-5" class="nba-board-red">
                    <div class="nba-text-container">
                        <div class="nba-text">A</div>
                    </div>
                </div>
                <div id="nba-board-6" class="nba-board-red">
                    <div class="nba-text-container">
                        <div class="nba-text">N</div>
                    </div>
                </div>
                <div id="nba-player-container">
                    <div id="nba-player">
                        <div id="nba-player-frame"></div>
                        <div id="nba-player-eyes"></div>
                    </div>
                </div>
                <div id="nba-ball"></div>
                <div class="ribbon-container">
                    <div class="ribbon-relative">
                        <div class="ribbon-left"></div>
                        <div class="ribbon-middle">开始热爱运动</div>
                        <div class="ribbon-right"></div>
                    </div>
                </div>
            </div>
             
            
            <div id="buildings-container">
            
            	<div id="tree-11" class="tree-bright-b"></div>
                <div id="tree-12" class="tree-dark-b"></div>
                <div id="tree-13" class="tree-bright-b"></div>
                <div id="tree-14" class="tree-dark-b"></div>
                <div id="tree-15" class="tree-bright-b"></div>
                <div id="tree-16" class="tree-dark-b"></div>
                <div id="tree-17" class="tree-bright-b"></div>
                <div id="tree-18" class="tree-dark-b"></div>
                <div id="tree-19" class="tree-bright-b"></div>
            
                <div id="building-1" class="building">
                    <div class="building-enemy-face-a">
                        <div class="building-enemy-face-a-eyes"></div>
                    </div>
                    <div id="building-leg-container-1" class="building-leg-container-a">
                        <div id="building-1-leg-frame" class="building-leg-frame-a"></div>
                    </div>
                </div>
                
                <div id="building-2" class="building"> 
                    <div class="building-enemy-face-b">
                        <div class="building-enemy-face-b-eyes"></div>
                    </div>   
                    <div id="building-leg-container-2" class="building-leg-container-b">
                        <div id="building-2-leg-frame" class="building-leg-frame-b"></div>
                    </div>
                </div>
                
                <div id="building-3" class="building">
                    <div class="building-enemy-face-a">
                        <div class="building-enemy-face-a-eyes"></div>
                    </div>
                    <div id="building-leg-container-3"  class="building-leg-container-a">
                        <div id="building-3-leg-frame"  class="building-leg-frame-a"></div>
                    </div>
                </div>
                
                <div class="ribbon-container">
                    <div class="ribbon-relative">
                        <div class="ribbon-left"></div>
                        <div class="ribbon-middle">我去湘湖跑了3次5km,每次相当于绕操场跑12圈</div>
                      
                        <div class="ribbon-right"></div>
  <div class="banner-button">
                        	
			<a rel="example_group1" href="http://smartmin.oss-cn-shanghai.aliyuncs.com/run.png" title="这就是我每次跑的线路，有兴趣的小朋友可以跟我一起跑"><img alt="" src="css/image/banner-button.png" /></a>
					<a rel="example_group1" href="http://smartmin.oss-cn-shanghai.aliyuncs.com/run2.jpeg" title="每次跑都特别累，但是真的很有成就感"></a>

                        </div>                      
                    </div>
                </div>
            </div>
            
            
            <div id="elevation-2" class="elevation">
                <div class="ground"></div>
                <div class="grass"></div>
            </div>
            
            
            <div id="ground-and-grass-container-1">
                <div class="ground"></div>
                <div class="grass"></div>
            </div>
            
            
            <div id="splash-container">
                <div id="scroll-or-swipe-text-container-1" class="scroll-or-swipe-text-container">请按方向键"↓"和"↑"控制我行走</div>
                <div id="scroll-or-swipe-text-container-2" class="scroll-or-swipe-text-container">Swipe from right to left</div>
              
                <div id="tree-5" class="tree-bright-d"></div>
                <div id="tree-6" class="tree-dark-b"></div>
                
                <div id="title-robby"></div>
                <div id="title-leonardi"></div>
                
                <div id="tree-1" class="tree-dark-c"></div>
                <div id="tree-2" class="tree-bright-e"></div>
                <div id="tree-3" class="tree-dark-b"></div>
                <div id="tree-4" class="tree-dark-a"></div>
                
                <div class="ribbon-container">
                    <div class="ribbon-relative">
                        <div class="ribbon-left"></div>
                        <div class="ribbon-middle">雷烨的冒险 2019</div>
                        <div class="ribbon-right"></div>
                    </div>
                </div>
            </div>
                     
            
            <div id="sea-wave-1" class="sea-wave"></div>
            
            
            <div id="sea-1" class="sea">
                <div id="title-skills" class="title-skills-class"></div>
                
                <div id="algae-1" class="algae-a"></div>
                <div id="algae-2" class="algae-b"></div>
                <div id="algae-3" class="algae-a"></div>
                <div id="algae-4" class="algae-b"></div>
                <div id="algae-5" class="algae-b"></div>
                <div id="algae-6" class="algae-a"></div>
                <div id="algae-7" class="algae-a"></div>
                <div id="algae-8" class="algae-b"></div>
               
                <div id="skill-1-container">
                 
                   
                    <div id="fish-text-container">
                        <div id="fish-ribbon-container-1" class="sea-ribbon-container">
                            <div class="sea-ribbon-left"></div>
                            <div class="sea-ribbon-middle">国画</div>
                            <div class="sea-ribbon-right"></div>
                        </div>
                        
                        <div id="fish-ribbon-container-2" class="sea-ribbon-container">
                            <div class="sea-ribbon-left"></div>
                            <div class="sea-ribbon-middle">写字</div>
                            <div class="sea-ribbon-right"></div>
                        </div>
                        
                  <%--      <div id="fish-ribbon-container-3" class="sea-ribbon-container">
                            <div class="sea-ribbon-left"></div>
                            <div class="sea-ribbon-middle">PHOTOSHOP</div>
                            <div class="sea-ribbon-right"></div>
                        </div>   
                        
                        <div id="fish-ribbon-container-4" class="sea-ribbon-container">
                            <div class="sea-ribbon-left"></div>
                            <div class="sea-ribbon-middle">FLASH</div>
                            <div class="sea-ribbon-right"></div>
                        </div> --%>        
                    </div>
                
                    <div id="fishes-container">
                        <div class="fish">
                            <div class="fish-eyes"></div>
                        </div>
                        <div class="fish">
                            <div class="fish-eyes"></div>
                        </div>
                        <div class="fish">
                            <div class="fish-eyes"></div>
                        </div>
                        <div class="fish">
                            <div class="fish-eyes"></div>
                        </div>
                        <div class="fish">
                            <div class="fish-eyes"></div>
                        </div>
                        <div class="fish">
                            <div class="fish-eyes"></div>
                        </div>
                        <div class="fish">
                            <div class="fish-eyes"></div>
                        </div>
                       
                        
                    </div>
                </div>
                
                <div id="skill-2-container">
               
                
                    <div id="crab-text-container">
                       
                    </div>
                    
                    <div id="crabs-container">
                        <div class="crab">
                            <div class="crab-eyes"></div>
                        </div>
                        <div class="crab">
                            <div class="crab-eyes"></div>
                        </div>
                        <div class="crab">
                            <div class="crab-eyes"></div>
                        </div>
                        <div class="crab">
                            <div class="crab-eyes"></div>
                        </div>
                        <div class="crab">
                            <div class="crab-eyes"></div>
                        </div>
                        <div class="crab">
                            <div class="crab-eyes"></div>
                        </div>
                        <div class="crab">
                            <div class="crab-eyes"></div>
                        </div>
                        <div class="crab">
                            <div class="crab-eyes"></div>
                        </div>
                        <div class="crab">
                            <div class="crab-eyes"></div>
                        </div>
                        <div class="crab">
                            <div class="crab-eyes"></div>
                        </div>
                        <div class="crab">
                            <div class="crab-eyes"></div>
                        </div>
                        <div class="crab">
                            <div class="crab-eyes"></div>
                        </div>
                        <div class="crab">
                            <div class="crab-eyes"></div>
                        </div>
                        <div class="crab">
                            <div class="crab-eyes"></div>
                        </div>
              
                    </div>
                </div>
                
                <div id="skill-3-container">
                    <div id="turtle-text-container">
                   
                    </div>
                
                    <div id="turtles-container">
                        <div class="turtle">
                            <div class="turtle-eyes"></div>
                        </div>
                        <div class="turtle">
                            <div class="turtle-eyes"></div>
                        </div>
                        <div class="turtle">
                            <div class="turtle-eyes"></div>
                        </div>
                        <div class="turtle">
                            <div class="turtle-eyes"></div>
                        </div>
                        <div class="turtle">
                            <div class="turtle-eyes"></div>
                        </div>
                        <div class="turtle">
                            <div class="turtle-eyes"></div>
                        </div>
                        
                      
                    </div>
                </div>
                
                <div id="bubble-container">
                    <div id="bubble"></div>
                </div> 
            </div>
            
            <div id="sea-floor"></div>
            
			<div id="ribbon-2-1" class="ribbon-absolute">
                <div class="ribbon-left"></div>
                <div class="ribbon-middle">开始学习国画与练字</div>
                <div class="ribbon-right"></div>
                   <div class="banner-button">
                            <a rel="example_group2" href="http://smartmin.oss-cn-shanghai.aliyuncs.com/pic1.jpeg" title="这是我画的水仙"><img alt="" src="css/image/banner-button.png" /></a>
					<a rel="example_group2" href="http://smartmin.oss-cn-shanghai.aliyuncs.com/pic2.jpeg" title="这是我画的枇杷"></a>

                        </div>
            </div>
                 
            

         

            
            <div id="panel-and-floor-container">
                <div id="panel"></div>
                <div id="floor"></div>
            </div>
            
            <div id="title-experience"></div>
            
            <div id="boxes"></div>
            
            <div id="experience-1-container">
            	<div class="chain-block-and-string-container">
                    <div class="chain-block-string"></div>
                    <div class="chain-block"></div>
            	</div>
                
                <div id="experience-text-container-1" class="experience-text-container">
                    <div class="experience-text-a">我的Scratch编程</div>
                    <div class="experience-text-b">孙悟空躲火球</div>
                    <div class="experience-text-d">我设计了好几个火球，它们一个个飞过来的时候我用编程把孙悟空变小了，这样就可以轻松躲开火球了，是不是很有趣啊？如果你们也喜欢的话，我可以教你们做。</div>
                        <div class="banner-button">
                    <a id="various55" href="https://smartmin.oss-cn-shanghai.aliyuncs.com/bf81b1c065a4e351921b41f5322e6384.mp4"><img src="css/image/banner-button.png"/></a>        	
                        </div> 
                </div>
            
                <div id="robot">
                	<div id="robot-body"></div>
                    
                    <div id="robot-hand-left">
                    	<div class="robot-hand-a"></div>
                        <div class="robot-hand-b"></div>
                        <div class="robot-hand-c"></div>
                        <div class="robot-hand-d"></div>
                    </div>
                    
                    <div id="robot-hand-right">
                    	<div class="robot-hand-a"></div>
                        <div class="robot-hand-b"></div>
                        <div class="robot-hand-c"></div>
                        <div class="robot-hand-d"></div>
                    </div>
                    
                    <div id="piechart-aol" class="piechart">
                        <div class="piechart-back"></div>
                        
                        <div id="piechart-aol-text-graphic-1" class="piechart-text-c">
                        
                        </div>
                    </div>
                </div>
                
              
            </div>
            
            <div id="elevation-3" class="elevation">
            	<div class="elevation-box"></div>
                <div class="elevation-floor"></div>
            </div>
            
            <div id="experience-2-container">
                <div class="chain-block-and-string-container">
                    <div class="chain-block-string"></div>
                    <div class="chain-block"></div>
            	</div>
                
                <div id="experience-text-container-2" class="experience-text-container">
                    <div class="experience-text-a">Scratch编程</div>
                    <div class="experience-text-b"></div>
                    <div class="experience-text-c">这是我在编程的学习过程，真的很复杂，但是也很有成就感。</div>
                            <div class="banner-button">
                            <a rel="example_group3" href="https://smartmin.oss-cn-shanghai.aliyuncs.com/subject.jpg" title="看这个逻辑图，是不是很复杂。"><img alt="" src="css/image/banner-button.png" /></a>

                        </div>
                </div>
            
                <div id="squid">
                    <div id="squid-hand-open-1" class="squid-hand-open"></div>
                    <div id="squid-hand-open-2" class="squid-hand-open"></div>
                    <div id="squid-hand-open-3" class="squid-hand-open"></div>
                    <div id="squid-hand-open-4" class="squid-hand-open"></div>
                    
                    <div id="squid-hand-close-1" class="squid-hand-close"></div>
                    <div id="squid-hand-close-2" class="squid-hand-close"></div>
                    <div id="squid-hand-close-3" class="squid-hand-close"></div>
                    <div id="squid-hand-close-4" class="squid-hand-close"></div>
                    
                    <div id="squid-body"></div>
                
                    <div id="piechart-incognito" class="piechart">
                        <div class="piechart-back"></div>
                        <div id="piechart-incognito-front"></div>
                        
                      <%--  <div id="piechart-incognito-text-graphic-1" class="piechart-text-a">GRAPHIC</div>
                        <div id="piechart-incognito-text-graphic-2" class="piechart-text-b">15%</div>
                        
                        <div id="piechart-incognito-text-animation-1" class="piechart-text-c">ANIMATION</div>
                        <div id="piechart-incognito-text-animation-2" class="piechart-text-d">70%</div>
                        
                        <div id="piechart-incognito-text-code-1" class="piechart-text-a">CODE</div>
                        <div id="piechart-incognito-text-code-2" class="piechart-text-b">15%</div>--%>
                    </div>
                </div>
                
               
            </div>
            
            <div id="elevation-4" class="elevation">
            	<div class="elevation-box"></div>
                <div class="elevation-floor"></div>
            </div>
            
            <div id="experience-3-container">
            	<div class="chain-block-and-string-container">
                    <div class="chain-block-string"></div>
                    <div class="chain-block"></div>
            	</div>
                
                <div id="experience-text-container-3" class="experience-text-container">
                    <div class="experience-text-a">未来的编程之路</div>
                    <div class="experience-text-b">FUTURE</div>
                    <div class="experience-text-e">未来我将要学习更多编程知识。有兴趣的可以来找我，我可以教你。</div>
                </div>
                
                <div id="alien">
                	<div id="alien-body"></div>
                    <div id="alien-steer"></div>
                    <div id="alien-ship"></div>
                
                    <div id="piechart-foxnews" class="piechart">
                        <div class="piechart-back"></div>
                        <div id="piechart-foxnews-front"></div>
                        
                  <%--      <div id="piechart-foxnews-text-graphic-1" class="piechart-text-c">GRAPHIC</div>
                        <div id="piechart-foxnews-text-graphic-2" class="piechart-text-d">70%</div>
                        
                        <div id="piechart-foxnews-text-animation-1" class="piechart-text-a">ANIMATION</div>
                        <div id="piechart-foxnews-text-animation-2" class="piechart-text-b">15%</div>
                        
                        <div id="piechart-foxnews-text-code-1" class="piechart-text-a">CODE</div>
                        <div id="piechart-foxnews-text-code-2" class="piechart-text-b">15%</div>--%>
                    </div>
                </div>
             
            </div>
            
            <div id="gate-3" class="gate">
                <div id="gate-text-3" class="gate-text">编程</div>
            </div>
            
            <div id="title-awards-and"></div>
            <div id="title-publication"></div>
            
            <div id="gate-4" class="gate">
                <div id="gate-text-4" class="gate-text">作品</div>
            </div>
        
            <div id="dock-column"></div>
            
            <div id="sea-wave-2" class="sea-wave"></div>
            
            <div id="sea-2" class="sea"></div>
            
            <div id="dock-floor"></div>
        </div>
        
    
        <div id="layer-vertical-3" class="layer-vertical">
            <div id="banners-container">
         
            
                
            
                <div class="banner">
                    <div class="banner-top-a"></div>
                    <div class="banner-middle-a">
                        <div class="banner-text-a">2019开启我的编程之路</div>
                        <div class="banner-line"></div>
                        <div class="banner-text-b">SCRATCH编程</div>
                        <div class="banner-line"></div>
                        <div class="banner-text-c">过程是枯燥的，但是看着自己的作品，真的很自豪呢。</div>
                              <div class="banner-button">
                                         <a id="various551" href="https://smartmin.oss-cn-shanghai.aliyuncs.com/bf81b1c065a4e351921b41f5322e6384.mp4"><img src="css/image/banner-button.png"/></a>        	


                        </div>
                    </div>
                    <div class="banner-bottom-a"></div>
                </div>
            
                <div class="banner">
                    <div class="banner-top-b"></div>
                    <div class="banner-middle-b">
                        <div class="banner-text-a">2019年开始学习国画</div>
                        <div class="banner-line"></div>
                        <div class="banner-text-b">绘画、国画</div>
                        <div class="banner-line"></div>
                        <div class="banner-text-c">这些美丽的国画里其实有很多技巧，还帮助了我平时的书写越来越工整了。我要争取参加明年的艺术节</div>
                        <div class="banner-button">
                            <a rel="example_group6" href="http://smartmin.oss-cn-shanghai.aliyuncs.com/pic1.jpeg" title=""><img alt="" src="css/image/banner-button.png" /></a>
					<a rel="example_group6" href="http://smartmin.oss-cn-shanghai.aliyuncs.com/pic2.jpeg" title=""></a>

                        </div>
                    </div>
                    <div class="banner-bottom-b"></div>
                </div>
            
                <div class="banner">
                    <div class="banner-top-a"></div>
                    <div class="banner-middle-a">
                        <div class="banner-text-a">2019/10/19</div>
                        <div class="banner-line"></div>
                        <div class="banner-text-b">我绕湘湖跑了5000米</div>
                        <div class="banner-line"></div>
                        <div class="banner-text-c">这就是我经常跑步的线路，每次跑完特别累可是也很开心。</div>
                        <div class="banner-button">
                        	
			<a rel="example_group7" href="http://smartmin.oss-cn-shanghai.aliyuncs.com/run.png" title="这就是我每次跑的线路，有兴趣的小朋友可以跟我一起跑"><img alt="" src="css/image/banner-button.png" /></a>
					<a rel="example_group7" href="http://smartmin.oss-cn-shanghai.aliyuncs.com/run2.jpeg" title="每次跑都特别累，但是真的很有成就感"></a>

                        </div>
                    </div>
                    <div class="banner-bottom-a"></div>
                </div>
          
            </div>
        </div>
        
        <div id="fireworks-container">
        	<div id="firework-container-1" class="firework-container">
            	<div class="firework"></div>
            </div>
            
            <div id="firework-container-2" class="firework-container">
            	<div class="firework"></div>
            </div>
            
            <div id="firework-container-3" class="firework-container">
            	<div class="firework"></div>
            </div>
            
            <div id="firework-container-4" class="firework-container">
            	<div class="firework"></div>
            </div>
            
            <div id="firework-container-5" class="firework-container">
            	<div class="firework"></div>
            </div>
            
            <div id="firework-container-6" class="firework-container">
            	<div class="firework"></div>
            </div>
        </div>  
        
        <div id="robby-container">
            <div id="robby">
                <div id="robby-slides"></div>
                <div id="robby-eyes-close"></div>
            </div>
        </div>
        
        <div id="balloon"></div>
        
        <div id="contact-container">
                                   
                    <div id="contact-center">
                        
                        <div id="contact-box">
                     
                                <div class="banner-text-end">请大家支持我！我是成长生雷烨，选我选我选我！❤️
</div>
                        </div>
                        
                      
                    </div>
                    
                    
                    
                    
                   
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                
                    <div id="social-container">
                        <div id="social-top"></div>
                        <div id="social-middle">
                            <div id="social-facebook" class="social-icon">
                            	<a href="" target="_blank">
                            	</a>
                            </div>
                            
                            <div id="social-twitter" class="social-icon">
                            	<a href="" target="_blank">
                            	</a>
                            </div>
                            
                            <div id="social-dribbble" class="social-icon">
                            	<a href="" target="_blank">
                            	</a>
                            </div>
                        </div>
                        <div id="social-bottom"></div>
                    </div>
                    
                    <div class="contact-confirmation-container">
                    	<div class="contact-confirmation">
                            <div class="contact-confirmation-rectangle"></div>
                            <div class="contact-confirmation-triangle"></div>
                            <div class="contact-confirmation-title-b">ERROR</div>
                            <div class="contact-confirmation-text-b">Please enter a valid email address!</div>
                        </div>
                    </div>
                    
                    <div class="contact-confirmation-container">
                    	<div class="contact-confirmation">
                            <div class="contact-confirmation-rectangle"></div>
                            <div class="contact-confirmation-triangle"></div>
                            <div class="contact-confirmation-title-b">ERROR</div>
                            <div class="contact-confirmation-text-b">Please fill out all of the information!</div>
                        </div>
                    </div>
                    
                    <div class="contact-confirmation-container">
                    	<div class="contact-confirmation">
                            <div class="contact-confirmation-rectangle"></div>
                            <div class="contact-confirmation-triangle"></div>
                            <div class="contact-confirmation-text-d">Sending...</div>
                        </div>
                    </div>
                    
                    <div class="contact-confirmation-container">
                    	<div class="contact-confirmation">
                            <div class="contact-confirmation-rectangle"></div>
                            <div class="contact-confirmation-triangle"></div>
                            <div class="contact-confirmation-title-b">ERROR</div>
                            <div class="contact-confirmation-text-c">Something wrong. Please try again!</div>
                        </div>
                    </div>
                    
                    <div class="contact-confirmation-container">
                    	<div class="contact-confirmation">
                            <div class="contact-confirmation-rectangle"></div>
                            <div class="contact-confirmation-triangle"></div>
                            <div class="contact-confirmation-title-a">THANK YOU</div>
                            <div class="contact-confirmation-text-a">Your message has been sent. I will get back to you as soon as possible!</div>
                        </div>
                    </div>
                    
                    <div id="contact-cloud-top"></div>
                    <div id="contact-cloud-bottom"></div>
                    <div id="contact-cloud-middle">
                    	<div id="contact-cloud-seal-top"></div>
                        <div id="contact-cloud-seal-bottom"></div>
                    </div>
        </div>
        
        <div id="detect-container">
          

<%--<div id="wrapper" >

	<audio preload="auto" controls autoplay loop>
		<source src="audio/孙俪-爱如空气.mp3">
		

	</audio>

</div>--%>


<script type="text/javascript" src="js1/audioplayer.js"></script>
<script type="text/javascript">
$(function(){
	$( 'audio' ).audioPlayer();
});
</script>
        </div>
        
             <div id="detect-container1">  
  
    </div>
        <div id="loginmodal" style="display:none;">
	<h1>User Login</h1>
	<form id="loginform" name="loginform" method="post" action="index.html">
		<label for="username">Username:</label>
		<input type="text" name="username" id="username" class="txtfield" tabindex="1" />
	
		<label for="password">Password:</label>
		<input type="password" name="password" id="password" class="txtfield" tabindex="2" />
	
		<div class="center"><input type="submit" name="loginbtn" id="loginbtn" class="flatbtn-blu hidemodal" value="Log In" tabindex="3"></div>
	</form>
</div>
    </div>
</div>
<!--这是主样式的email的js-->
    <script type="text/javascript" src="script/email.js"></script>
    <script type="text/javascript" src="script/main.js"></script>
<!--这是Iframe的样式-->
    <script src="js3/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="js3/jquery.fancybox-1.3.4.js" type="text/javascript"></script>

<!--鼠标控制滚动-->
<link rel="stylesheet" type="text/css" href="css3/jquery.fancybox-1.3.4.css" media="screen" />
<!--这是Iframe的样式-->
<script type="text/javascript">
        $$(document).ready(function() {

		$$("a#example1").fancybox();

		$$("a#example2").fancybox({
			'overlayShow':false,
			'transitionIn':'elastic',
			'transitionOut':'elastic'
		});

		$$("a#example3").fancybox({
			'transitionIn':'none',
			'transitionOut':'none'	
		});

		$$("a#example4").fancybox({
			'opacity':true,
			'overlayShow':false,
			'transitionIn':'elastic',
			'transitionOut':'none'
		});

		$$("a#example5").fancybox();

		$$("a#example6").fancybox({
			'titlePosition':'outside',
			'overlayColor':'#000',
			'overlayOpacity':0.9
		});

		$$("a#example7").fancybox({
			'titlePosition':'inside'
		});

		$$("a#example8").fancybox({
			'titlePosition':'over'
		});

		$$("a[rel=example_group]").fancybox({
			'transitionIn':'none',
			'transitionOut':'none',
			'titlePosition':'over',
			'titleFormat':function(title, currentArray, currentIndex, currentOpts) {
				return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
			}
		});

		$$("a[rel=example_group1]").fancybox({
		    'transitionIn': 'none',
		    'transitionOut': 'none',
		    'titlePosition': 'over',
		    'titleFormat': function (title, currentArray, currentIndex, currentOpts) {
		        return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
		    }
		});

		$$("a[rel=example_group2]").fancybox({
		    'transitionIn': 'none',
		    'transitionOut': 'none',
		    'titlePosition': 'over',
		    'titleFormat': function (title, currentArray, currentIndex, currentOpts) {
		        return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
		    }
		});
		$$("a[rel=example_group3]").fancybox({
		    'transitionIn': 'none',
		    'transitionOut': 'none',
		    'titlePosition': 'over',
		    'titleFormat': function (title, currentArray, currentIndex, currentOpts) {
		        return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
		    }
		});

		$$("a[rel=example_group6]").fancybox({
		    'transitionIn': 'none',
		    'transitionOut': 'none',
		    'titlePosition': 'over',
		    'titleFormat': function (title, currentArray, currentIndex, currentOpts) {
		        return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
		    }
		});

		$$("a[rel=example_group7]").fancybox({
		    'transitionIn': 'none',
		    'transitionOut': 'none',
		    'titlePosition': 'over',
		    'titleFormat': function (title, currentArray, currentIndex, currentOpts) {
		        return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
		    }
		});
		$$("#various1").fancybox({
			'titlePosition':'inside',
			'transitionIn':'none',
			'transitionOut':'none'
		});

		$$("#various2").fancybox();

		$$("#various3").fancybox({
			'width':'75%',
			'height':'75%',
			'autoScale':false,
			'transitionIn':'none',
			'transitionOut':'none',
			'type':'iframe'
		});
		
		$$("#various5").fancybox({
			'width':'75%',
			'height':'75%',
			'autoScale':false,
			'transitionIn':'none',
			'transitionOut':'none',
			'type':'iframe'
		});
		$$("#various55").fancybox({
		    'width': '75%',
		    'height': '75%',
		    'autoScale': false,
		    'transitionIn': 'none',
		    'transitionOut': 'none',
		    'type': 'iframe'
		});
		$$("#various551").fancybox({
		    'width': '75%',
		    'height': '75%',
		    'autoScale': false,
		    'transitionIn': 'none',
		    'transitionOut': 'none',
		    'type': 'iframe'
		});


	$$("#various6").fancybox({
			'width':'75%',
			'height':'75%',
			'autoScale':false,
			'transitionIn':'none',
			'transitionOut':'none',
			'type':'iframe'
		});
		
		$$("#various7").fancybox({
			'width':'95%',
			'height':'95%',
			'autoScale':false,
			'transitionIn':'none',
			'transitionOut':'none',
			'type':'iframe'
		});
		

		$$("#various4").fancybox({
			'padding':0,
			'autoScale':false,
			'transitionIn':'none',
			'transitionOut':'none'
		});
	});
	

	
</script>


 





</body>
</html>