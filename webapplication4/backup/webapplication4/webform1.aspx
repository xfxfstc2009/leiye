﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication4.WebForm1" %>

<!-- Copyright hackerzhou.me -->
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>爱豆豆</title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<style type="text/css">
	
	</style>
	<link href="css/default.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/garden.js"></script>
    <script type="text/javascript" src="js/functions.js"></script>

</head>

<body>

	
			<div id="code">
				<span class="comments">小敏</span><br />
				<span class="space"/><span class="comments">* 几年前刚学会编程时,答应给你做个网站却直到我们结婚还没有实现 </span><br />
				<span class="space"/><span class="comments">* 所以，现在这个网站就是给你做的.</span><br />
				<span class="space"/><span class="comments">*/</span><br />
				Boy i = <span class="keyword">new</span> Boy(<span class="string">&quot;杨鸿杰&quot;</span>);<br />
				Girl u = <span class="keyword">new</span> Girl(<span class="string">&quot;马茜&quot;</span>);<br />
				<span class="comments">// 2009年1月4号我告诉你我爱你. </span><br />
				i.love(u);<br />
				<span class="comments">// 然后你同意了.</span><br />
				u.accepted();<br />
				<span class="comments">// 不在你身边日子会想念你.</span><br />
				i.miss(u);<br />
				<span class="comments">// 以后的日子我来照顾你.</span><br />
				i.takeCareOf(u);<br />
				<span class="comments">// 以前在一起的时候，总是傻傻的什么都不懂.</span><br />
				<span class="comments">// 在有宝宝的时候，我们有犹豫过.</span><br />
				<span class="keyword">boolean</span> isHesitate = <span class="keyword">true</span>;<br />
				<span class="keyword">while</span> (isHesitate) {<br />
				<span class="placeholder"/>i.wantMarry(u);<br />
				<span class="placeholder"/><span class="comments">// 这是一个重要的决定</span><br />
				<span class="placeholder"/><span class="comments">// 我们都想好了.</span><br />
				<span class="placeholder"/>isHesitate = our.thinkOver();<br />
				}<br />
				<span class="comments">// 一个浪漫的婚礼后，我们会永远幸福地生活.</span><br />
				i.marry(u);<br />
				i.liveHappilyWith(u);<br />
			</div>
			
				<canvas id="garden"></canvas>
				
	
		
	
	<script type="text/javascript">
		var offsetX = $("#loveHeart").width() / 2;
		var offsetY = $("#loveHeart").height() / 2 - 55;
		var together = new Date();
		together.setFullYear(2009, 1, 4);
		together.setHours(3);
		together.setMinutes(0);
		together.setSeconds(0);
		together.setMilliseconds(0);
		
		if (!document.createElement('canvas').getContext) {
			var msg = document.createElement("div");
			msg.id = "errorMsg";
			msg.innerHTML = "Your browser doesn't support HTML5!<br/>Recommend use Chrome 14+/IE 9+/Firefox 7+/Safari 4+"; 
			document.body.appendChild(msg);
			$("#code").css("display", "none")
			$("#copyright").css("position", "absolute");
			$("#copyright").css("bottom", "10px");
		    document.execCommand("stop");
		} else {
			setTimeout(function () {
				startHeartAnimation();
			}, 5000);

			timeElapse(together);
			setInterval(function () {
				timeElapse(together);
			}, 50);

			adjustCodePosition();
			$("#code").typewriter();
		}
	</script>
</body>
</html>
