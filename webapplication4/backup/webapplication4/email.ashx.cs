﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;


namespace WebApplication4
{  /// <summary>
    /// Email1 的摘要说明
    /// </summary>
    public class Email1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/html";
            string subject = context.Request.Params["email-subject"];
            string content = context.Request.Params["email-message"];



            //声明一个Mail对象
            MailMessage mymail = new MailMessage();
            //发件人地址
            mymail.From = new MailAddress("a1210444520@sina.com");
            //收件人地址
            mymail.To.Add(new MailAddress("1210444520@qq.com"));
            //邮件主题
            mymail.Subject = subject;
            //发送邮件的内容
            mymail.Body = content;
            //创建一个邮件服务器类
            SmtpClient myclient = new SmtpClient();
            myclient.Host = "smtp.sina.com";
            //SMTP服务端口
            myclient.Port = int.Parse("25");
            //验证登录
            myclient.Credentials = new NetworkCredential("a1210444520", "aq123456");
            myclient.Send(mymail);
      
        }





        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}